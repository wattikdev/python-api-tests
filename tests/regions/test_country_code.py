import math

import pytest
from testfixtures import compare

from framework.constants.responses import Responses
from tests.regions.test_query import get_error_request_model, get_successful_request_model


def test_country_code_is_empty(api_client):
    query_model = get_error_request_model(api_client, country_code='')
    assert Responses.country_code_must_be_set == query_model.error.message


@pytest.mark.parametrize(
    argnames="code",
    argvalues=[
        "ru",
        "kg",
        "kz",
        "cz"
    ])
def test_listed_country_code(api_client, code):
    query_model = get_successful_request_model(api_client, country_code=code)
    for item in query_model.items:
        assert code == item.country.code


@pytest.mark.parametrize(
    argnames="code",
    argvalues=[
        "ua",
        "au",
        "gr",
        "ge"
    ])
def test_country_code_not_listed(api_client, code):
    query_model = get_error_request_model(api_client, country_code=code)
    assert Responses.country_code_must_be_set == query_model.error.message


def test_country_code_default_value(api_client):
    expected_result = ["ru", "kg", "kz", "cz"]
    actual_result = []
    total_count = api_client.get_cities_by_query_params()['total']
    count = math.ceil(total_count/10) + 1
    for i in range(1, count):
        query_model = get_successful_request_model(api_client, page=i, page_size=10)
        for item in query_model.items:
            actual_result.append(item.country.code)

    compare(set(expected_result), set(actual_result))


@pytest.mark.parametrize(
    argnames="number",
    argvalues=[
        "0",
        "1",
        "77",
        "54",
        "10.0"
    ])
def test_country_code_numeric_value(api_client, number):
    query_model = get_error_request_model(api_client, country_code=number)
    assert Responses.country_code_must_be_set == query_model.error.message


@pytest.mark.parametrize(
    argnames="special_characters",
    argvalues=[
        "@",
        "!",
        "$",
        "%",
        "^",
        "&",
        "*",
        "!@#$%^&*"
    ])
def test_country_code_special_characters(api_client, special_characters):
    query_model = get_error_request_model(api_client, country_code=special_characters)
    assert Responses.country_code_must_be_set == query_model.error.message
