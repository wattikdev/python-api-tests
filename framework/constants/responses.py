

class Responses:
    more_than_3_characters: str = "Параметр 'q' должен быть не менее 3 символов"
    parameter_must_be_string_value: str = "Параметр 'q' должен быть строковым значением"
    country_code_must_be_set: str = "Параметр 'country_code' может быть одним из следующих значений: ru, kg, kz, cz"
    page_parameter_must_be_an_integer: str = "Параметр 'page' должен быть целым числом"
    page_parameter_should_not_be_0: str = "Параметр 'page' не должен быть 0"
    page_size_parameter_must_be_an_integer: str = "Параметр 'page_size' должен быть целым числом"
    page_size_parameter_should_be_from_the_list: str = \
        "Параметр 'page_size' может быть одним из следующих значений: 5, 10, 15"
