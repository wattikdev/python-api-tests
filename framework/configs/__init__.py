from configparser import ConfigParser
from attrdict import AttrDict
from os import path


def get_configs(file_names) -> AttrDict:
    result_config = AttrDict()
    config_parser = ConfigParser(dict_type=AttrDict)
    for file_name in file_names:
        config_parser.read(path.dirname(__file__) + '/' + file_name)
        result_config += config_parser._sections

    return result_config


config = get_configs(('config.ini',))
