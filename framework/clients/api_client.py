import logging

import requests

from framework.clients.http_client_base import HttpClientBase, RequestType

log = logging.getLogger(__name__)


class ApiClient(HttpClientBase):
    def __init__(self, base_api_url):
        HttpClientBase.__init__(self, base_api_url)
        self.base_uri = '/1.0/regions'

    def _log_response_from_api(self, response: requests.Response):
        if response.content:
            response_text = self._prettify_json(response.content.decode(response.apparent_encoding))
        else:
            response_text = response.text

        response_text = "Response from API:\n{}".format(response_text)
        log.info(response_text)

    def get_cities_by_query_params(self, q=None, country_code=None, page=None, page_size=None):
        log.debug(f'Trying to execute a request with parameters '
                  f'q={q}, country_code={country_code}, page={page}, page_size={page_size}')
        headers = {"Content-type": "application/json; charset=utf-8"}
        payload = {
            'q': q,
            'country_code': country_code,
            'page': page,
            'page_size': page_size
        }
        response = self.request(RequestType.GET, self.base_uri, headers=headers, params=payload)

        response.raise_for_status()
        self._log_response_from_api(response)

        return response.json()
