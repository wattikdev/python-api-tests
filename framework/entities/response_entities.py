from dataclasses import dataclass, asdict
from typing import List


@dataclass
class CountryInfo:
    name: str = None
    code: str = None

    def get_raw_data(self):
        return asdict(self)

    def __eq__(self, other):
        if other.__class__ is not self.__class__:
            raise TypeError()
        if other.name != self.name:
            return False
        if other.code != self.code:
            return False
        return True


@dataclass
class ItemInfo:
    id: int = None
    name: str = None
    code: str = None
    country: CountryInfo = None

    def get_raw_data(self):
        return asdict(self)

    def __eq__(self, other):
        if other.__class__ is not self.__class__:
            raise TypeError()
        if other.id != self.id:
            return False
        if other.name != self.name:
            return False
        if other.code != self.code:
            return False
        if other.country != self.country:
            return False
        return True


@dataclass
class QueryInfo:
    total: int
    items: List[ItemInfo]

    def get_raw_data(self):
        return asdict(self)

    def __eq__(self, other):
        if other.__class__ is not self.__class__:
            raise TypeError()
        if other.total != self.total:
            return False
        if other.items != self.items:
            return False
        return True


@dataclass
class QueryErrorMessage:
    id: str
    message: str

    def get_raw_data(self):
        return asdict(self)

    def __eq__(self, other):
        if other.__class__ is not self.__class__:
            raise TypeError()
        if other.id != self.id:
            return False
        if other.message != self.message:
            return False
        return True


@dataclass
class QueryError:
    error: QueryErrorMessage

    def get_raw_data(self):
        return asdict(self)

    def __eq__(self, other):
        if other.__class__ is not self.__class__:
            raise TypeError()
        if other.error != self.error:
            return False
        return True
